create table client (
    id bigint auto_increment primary key,
    fullname varchar(128),
    abbreviation varchar(6)
)