create table staff (
    id bigint auto_increment primary key ,
    firstname varchar(32),
    lastname varchar(32),
    officeId bigint,
    foreign key (officeId) references office(id)
)