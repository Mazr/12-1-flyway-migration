alter table contract add serviceId bigint;
alter table contract add foreign key (serviceId) references service(id);