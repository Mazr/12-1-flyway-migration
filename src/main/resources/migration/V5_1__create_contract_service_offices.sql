create table contract_service_offices(
    officeId  bigint,
    contractId bigint,
    foreign key (officeId) references office(id),
    foreign key (contractId) references contract(id),
    primary key (officeId,contractId)
)