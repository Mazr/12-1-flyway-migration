create table office(
    id bigint auto_increment primary key ,
    country VARCHAR(64),
    city VARCHAR(64)
)