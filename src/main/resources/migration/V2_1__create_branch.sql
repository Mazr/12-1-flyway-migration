create table branch(
    id bigint auto_increment primary key ,
    name varchar(128),
    clientId bigint,
    foreign key (clientId) references client(id)
)