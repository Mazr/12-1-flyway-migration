create table contract(
    id bigint auto_increment primary key ,
    name varchar(256),
    branchId bigint,
    foreign key (branchId) references branch(id)
)